package com.jiradev.jira.plugins.panels.project.api;

public interface MyPluginComponent
{
    String getName();
}